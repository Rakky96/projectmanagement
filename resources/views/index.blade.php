@extends('template')

@section('front')


<div class="table">
  <div id="todo">
    @foreach($data as $task)
    @if($task->status === 'todo')
      <div class="item">
        <h5 class="card-title">{{$task->name}}</h5>
        {{-- <div>  Dodano przez: {{ $task->users[0]->name }} </div> --}}
        <a href="{{ url('show',$task->id) }}" class="btn btn-primary">Przejdź do zadania</a>
      </div>
    @endif
    @endforeach
  </div>

  <div id="doing">
    @foreach($data as $task)
    @if($task->status === 'doing')
      <div class="item">
        <h5 class="card-title">{{$task->name}}</h5>
        {{-- <div>  Dodano przez: {{ $task->users[0]->name }} </div> --}}
        <a href="{{ url('show',$task->id) }}" class="btn btn-primary">Przejdź do zadania</a>
      </div>
    @endif
    @endforeach
  </div>

  <div id="stoping">
    @foreach($data as $task)
    @if($task->status === 'stoping')
      <div class="item">
        <h5 class="card-title">{{$task->name}}</h5>
        {{-- <div>  Dodano przez: {{ $task->users[0]->name }} </div> --}}
        <a href="{{ url('show',$task->id) }}" class="btn btn-primary">Przejdź do zadania</a>
      </div>
    @endif
    @endforeach
  </div>

  <div id="done">
    @foreach($data as $task)
    @if($task->status === 'done')
      <div class="item">
        <h5 class="card-title">{{$task->name}}</h5>
        {{-- <div>  Dodano przez: {{ $task->users[0]->name }} </div> --}}
        <a href="{{ url('show',$task->id) }}" class="btn btn-primary">Przejdź do zadania</a>
      </div>
    @endif
    @endforeach
  </div>
</div>






@endsection